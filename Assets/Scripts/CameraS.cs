﻿using UnityEngine;

public class CameraS : MonoBehaviour
{
    private Transform player;
    private Vector3 offset;

    void Start() 
    {// Função para fazer com que a camera enxergue um objeto com a Tag Player e se reposicione
        player = GameObject.FindGameObjectWithTag("Player").transform;
        offset = transform.position - player.position;
    }

    void LateUpdate()
    {// A medida que o jogador vai se movimentando a camera precisa atualizar Atualizar o Eixo X e Y, só é passado o Eixo Z com os devidos offsets para manter a distancia em Z da camera para o personagem.
        Vector3 nPosicao = new Vector3(transform.position.x, transform.position.y, player.position.z + offset.z);
        transform.position = nPosicao;
    }
}
