﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gm;

    private void Awake()
    {// Função para Instanciar o Game Manager e / ou destrui-lo...
        if (gm == null)
            gm = this;
        else if (gm != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public void StartGame()
    {// Carregar a Scene Game
        SceneManager.LoadScene("Game");
    }
    public void Menu()
    {// Carregar a Scene Menu
        SceneManager.LoadScene("Menu");
    }
}
