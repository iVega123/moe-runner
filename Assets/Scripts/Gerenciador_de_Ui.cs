﻿using UnityEngine.UI;
using UnityEngine;

public class Gerenciador_de_Ui : MonoBehaviour
{
    public Image[] vida;

    public Text NumCol;

    public GameObject GOP;

    public Text txtPontos;

    public void UpdateVida(int vidas)
    {//  função que muda a cor dos corações no Canvas Principal
        for (int i = 0; i < vida.Length; i++)
        {
            if (vidas > i)
                vida[i].color = Color.white;
            else
                vida[i].color = Color.black;
        }
    }
    public void UpdateCol(int col)
    {// função que atualiza no Canvas os Peixes coletádos em Game
        NumCol.text = col.ToString();
    }

    public void AttPontos(int pontos)
    {// função que atualiza os pontos no Canvas.
        txtPontos.text = "Pontos: " + pontos + " m";
    }
}
