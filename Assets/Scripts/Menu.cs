﻿using UnityEngine;

public class Menu : MonoBehaviour
{
    public void StartGame()
    {// função que chama o Inicio do Game no Game Manager
        GameManager.gm.StartGame();
    }
    public void EndGame()
    {// função para fechar a aplicação
        Application.Quit();
    }
}
