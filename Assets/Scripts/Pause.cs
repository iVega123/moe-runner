﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class Pause : MonoBehaviour
{
    public static bool Pausa = false;

    public GameObject PausaMenu;

    void Start()
    {// Impede que o Canvas de Pause inicie na tela.
        PausaMenu.SetActive(false);   
    }
    void Update()
    {// Binda o Canvas do Pause na Tecla "ESC", e seta duas condições uma para Resumir e outra para Pausar.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Pausa)
                Resumir();
            else
                Pausar();
        }
    }
    public void Resumir()
    {// Função para Resumir o Jogo
        PausaMenu.SetActive(false);
        Time.timeScale = 1f;
        Pausa = false;
    }
    void Pausar()
    {// Função para Pausar o Jogo
        PausaMenu.SetActive(true);
        Time.timeScale = 0f;
        Pausa = true;
    }
    public void Menu()
    {// Função para Retornar ao Menu principal
        SceneManager.LoadScene("Menu");
    }
}
