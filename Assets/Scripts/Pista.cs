﻿using System.Collections.Generic;
using UnityEngine;

public class Pista : MonoBehaviour
{
    public GameObject[] obstaculos;
    public Vector2 ndo;
    public GameObject col;
    public Vector2 NoC;
    public List<GameObject> novos_obstaculos;
    public List<GameObject> novosCol;


    void Start()
    {// configuração de quantos obstaculos devem ser instanciados na cena, sendo controlado pelo editor pelas variáveis NoC para colétaveis e ndo para Obstaculos...
        int newndo = (int)Random.Range(ndo.x, ndo.y);
        int newNoC = (int)Random.Range(NoC.x, NoC.y);
        for (int i = 0; i < newndo; i++)
        {
            novos_obstaculos.Add(Instantiate(obstaculos[Random.Range(0, obstaculos.Length)], transform));
            novos_obstaculos[i].SetActive(false);
        }
        for (int i = 0; i < newNoC; i++)
        {
            novosCol.Add(Instantiate(col, transform));
            novosCol[i].SetActive(false);
        }
        PosicionarObstaculos();
        PosicionarColetaveis();
    }

    void PosicionarObstaculos()
    {// Posicionamento dos obstaculos nas lanes
        for (int i = 0; i < novos_obstaculos.Count; i++)
        {
            float posZmin = (675f / novos_obstaculos.Count) + (675f / novos_obstaculos.Count) * i;
            float posZmax = (675f / novos_obstaculos.Count) + (675f / novos_obstaculos.Count) * i + 1;
            novos_obstaculos[i].transform.localPosition = new Vector3(0, 0, Random.Range(posZmin, posZmax));
            novos_obstaculos[i].SetActive(true);
            if (novos_obstaculos[i].GetComponent<Troca_de_Linha_O>() != null)
                novos_obstaculos[i].GetComponent<Troca_de_Linha_O>().PosicaoLane();
        }
    }
    void PosicionarColetaveis()
    {// posicionamento dos coletáveis nas lanes
        float minZPos = 10f;
        for (int i = 0; i < novosCol.Count; i++)
        {
            float maxPosZ = minZPos + 5f;
            float randomZPos = Random.Range(minZPos, maxPosZ);
            novosCol[i].transform.localPosition = new Vector3(transform.position.x, transform.position.y, randomZPos);
            novosCol[i].SetActive(true);
            novosCol[i].GetComponent<Troca_de_Linha_O>().PosicaoLane();
            minZPos = randomZPos + 1;
        }
    }
    private void OnTriggerEnter(Collider other)
    {// reset da pista caso o Player atinja certo box collider configurado na cena..
        if (other.CompareTag("Player"))
        {
            other.GetComponent<Player>().AttVelocidade();
            transform.position = new Vector3(0, 0, transform.position.z + 670 * 2);
            PosicionarObstaculos();
            PosicionarColetaveis();
        }
    }
}
