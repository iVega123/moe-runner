﻿using System.Collections;
using UnityEngine;
public class Player : MonoBehaviour
{
    private Rigidbody corpo;
    private Animator animacao;
    public float velocidadedalane;
    public int vidaMax = 3;
    public float escorregar_tamanho;
    private int laneatual = 1;
    public float Velocidade;
    public float VelocidadeA = 0;
    public float minVeloc = 10f;
    public float maxVeloc = 30f;
    private bool pulando = false;
    public float tamanho_pulo;
    private BoxCollider bc;
    public float altura_pulo;
    private float spulo;
    private Vector3 vtp;
    private bool escorregando = false;
    private float sescorregar;
    private Vector3 bc_tamanho;
    private bool isSwipping = false;
    private Vector2 TouchS;
    private int vidaatual;
    private bool inv = false;
    static int BlinkV;
    public float invT;
    private Gerenciador_de_Ui Ui;
    private int col;
    private float pontos;
    public AudioSource AD;
    void Start()
    {// instanciamento dos objetos do Player principal, pegando os componentes e setando a animação de entrada
        corpo = GetComponent<Rigidbody>();
        animacao = GetComponentInChildren<Animator>();
        bc = GetComponent<BoxCollider>();
        AD = GetComponent<AudioSource>();
        bc_tamanho = bc.size;
        animacao.Play("runStart");
        vidaatual = vidaMax;
        if (VelocidadeA != 0)
            Velocidade = VelocidadeA;
        else
            Velocidade = minVeloc;
        BlinkV = Shader.PropertyToID("_BlinkingValue");
        Ui = FindObjectOfType<Gerenciador_de_Ui>();
    }

    void Update()
    {// atualização dos pontos, estados do player e lanes, juntamente com um input para touch Mobile, além de controle de pulos e escorregão e etc...
        pontos += Time.deltaTime * Velocidade;
        Ui.AttPontos((int)pontos);
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            TrocarLane(-1);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            TrocarLane(1);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Pulo();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Escorregar();
        }

        if(Input.touchCount == 1)
        {// Input de Mobile... Pegando o ponto que o usuário clicar na tela e mapeando sé para cima ou para baixo
            if (isSwipping)
            {
                Vector2 diff = Input.GetTouch(0).position - TouchS;
                diff = new Vector2(diff.x / Screen.width, diff.y / Screen.width);
                if(diff.magnitude > 0.01f)
                {
                    if (Mathf.Abs(diff.y) > Mathf.Abs(diff.x))
                    {
                        if (diff.y < 0)
                            Escorregar();
                        else
                            Pulo();
                    }
                    else
                    {
                        if (diff.x < 0)
                            TrocarLane(-1);
                        else
                            TrocarLane(1);
                    }
                    isSwipping = false;
                }
            }
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                TouchS = Input.GetTouch(0).position;
                isSwipping = true;
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                isSwipping = false;
            }
        }

      

        if (pulando)
        {// Pulo... mudando a animação e a posição em Y do personagem...
            float ratio = (transform.position.z - spulo) / tamanho_pulo;
            if(ratio >= 1f)
            {
                pulando = false;
                animacao.SetBool("Jumping", false);
            }
            else
            {
                vtp.y = Mathf.Sin(ratio * Mathf.PI) * altura_pulo;
            }
        }
        else
        {
            vtp.y = Mathf.MoveTowards(vtp.y, 0, 5 * Time.deltaTime);
        }
        if (escorregando)
        {// escorregar.... mudando a animação e castando o tamanho do escorregão.
            float ratio = (transform.position.z - sescorregar) / escorregar_tamanho;
            if(ratio >= 1f)
            {
                escorregando = false;
                animacao.SetBool("Sliding", false);
                bc.size = bc_tamanho;
            }
        }

        Vector3 posicaoalvo = new Vector3(vtp.x, vtp.y, transform.position.z);
        transform.position = Vector3.MoveTowards(transform.position, posicaoalvo, velocidadedalane * Time.deltaTime);
    }

    private void FixedUpdate()
    {// Função para aumentar a velocidade base do personagem
        corpo.velocity = Vector3.forward * Velocidade;
    }
    void TrocarLane(int direcao)
    {// Função para trocar a lane do personagem
        int lanealvo = laneatual + direcao;
        if(lanealvo < 0 || lanealvo > 2)
            return;
        laneatual = lanealvo;
        vtp = new Vector3((laneatual - 1), 0, 0);
    }
    void Pulo()
    {// Função para pular e só sendo possivel se o personagem não estiver pulando ou escorregando...e só sendo possivel se o personagem não estiver pulando ou escorregando...
        if (!pulando && !escorregando)
        {
            spulo = transform.position.z;
            animacao.SetFloat("JumpSpeed", Velocidade / tamanho_pulo);
            animacao.SetBool("Jumping", true);
            pulando = true;
        }
    }
    void Escorregar()
    {// escorregão... no caso mudando a animação e só sendo possivel se o personagem não estiver pulando ou escorregando...
        if(!pulando && !escorregando)
        {
            sescorregar = transform.position.z;
            animacao.SetFloat("JumpSpeed", Velocidade / escorregar_tamanho);
            animacao.SetBool("Sliding", true);
            Vector3 novotamanho = bc.size;
            novotamanho.y = novotamanho.y / 2;
            bc.size = novotamanho;
            escorregando = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {// Função que controla os hits nos obstaculos e nos colétaveis
        if (other.CompareTag("Coletável"))
        {// comparação caso o personagem colete o coletável
            col++;
            Ui.UpdateCol(col);
            other.transform.parent.gameObject.SetActive(false);
        }

        if (inv)
            return;
        if (other.CompareTag("Obstaculo"))
        {// comparação caso o personagem acerte o obstaculo.
            vidaatual--;
            Ui.UpdateVida(vidaatual);
            animacao.SetTrigger("Hit");
            Velocidade = 0;
            if(vidaatual <= 0)
            {// caso a vida total do personagem chegar a 0, triga uma animação e da game over...
                Velocidade = 0;
                animacao.SetBool("Dead", true);
                Ui.GOP.SetActive(true);
                AD.Stop();
                Invoke("Menu", 5f);
            }
            else
            {// faz com que o personagem começe a brilhar e ficar invunerável...
                StartCoroutine(Blinking(invT));
            }
        }
    }
    IEnumerator Blinking(float time)
    {// função que reseta a velocidade do player para 0, o deixa brilhando e invunerável.
        inv = true;
        float timer = 0;
        float cBlink = 1f;
        float lBlink = 0;
        float pBlink = 0.1f;
        yield return new WaitForSeconds(1f);
        if (VelocidadeA != 0)
            Velocidade = VelocidadeA;
        else
            Velocidade = minVeloc;
        while (timer < time && inv)
        {
            Shader.SetGlobalFloat(BlinkV, cBlink);
            yield return null;
            timer += Time.deltaTime;
            lBlink += Time.deltaTime;
            if(pBlink < lBlink)
            {
                lBlink = 0;
                cBlink = 1f - cBlink;
            }
        }
        Shader.SetGlobalFloat(BlinkV, 0);
        inv = false;
    }

    void Menu()
    {// Função para chamar o menu Principal
        GameManager.gm.Menu();
    }
    public void AttVelocidade()
    {// Função para atualizar a velocidade do player...
        VelocidadeA = Velocidade *= 1.15f;
        if (VelocidadeA >= maxVeloc)
            VelocidadeA = maxVeloc;
    }
}
