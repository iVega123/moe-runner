﻿using UnityEngine;

public class Troca_de_Linha_O : MonoBehaviour
{
    public void PosicaoLane()
    {// Função responsável por randomizar os obistaculos na Pista... Dando para eles um Range de -1 a 1, fazendo com que os objetos possam surgir da esquerda para a direita e também ao centro.
        int randomlane = Random.Range(-1, 2);
        transform.position = new Vector3(randomlane, transform.position.y, transform.position.z);
    }
}
